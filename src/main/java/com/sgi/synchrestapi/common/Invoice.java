package com.sgi.synchrestapi.common;


/**
 * @author Gary Russell
 * @since 2.2.1
 *
 */
public class Invoice {

    private String invoiceId;
    private String invoiceNo;
    private String policyNo;
    private String holderId;
    private String holderName;
    private long invoiceAmount;

    private boolean paymentStatus;

    public Invoice() {
        this.paymentStatus = false;
    }

    public Invoice(String invoiceId, String invoiceNo, String policyNo, String holderId, String holderName, long invoiceAmount) {
        this.invoiceId = invoiceId;
        this.invoiceNo = invoiceNo;
        this.policyNo = policyNo;
        this.holderId = holderId;
        this.holderName = holderName;
        this.invoiceAmount = invoiceAmount;
        this.paymentStatus = false;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getHolderId() {
        return holderId;
    }

    public void setHolderId(String holderId) {
        this.holderId = holderId;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public long getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(long invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String toString(){
        return ""+this.invoiceId+" "+this.invoiceNo;
    }

    public boolean isPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}