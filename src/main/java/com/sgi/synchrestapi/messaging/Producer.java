package com.sgi.synchrestapi.messaging;

import com.sgi.synchrestapi.InvoiceService;
import com.sgi.synchrestapi.common.Invoice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

public class Producer {
    private final Logger logger = LoggerFactory.getLogger(InvoiceService.class);

    @Autowired
    private KafkaTemplate<Object, Object> template;

    public void sendInvoice(Invoice invoice){
        this.template.send("invoice-topic", invoice.getInvoiceId(), invoice);
    }
}
