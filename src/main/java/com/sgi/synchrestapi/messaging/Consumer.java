package com.sgi.synchrestapi.messaging;

import com.sgi.synchrestapi.common.Invoice;
import com.sgi.synchrestapi.repository.InvoiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class Consumer {
    private final Logger logger = LoggerFactory.getLogger(Consumer.class);
    @Autowired
    private KafkaTemplate<Object, Object> template;

    @KafkaListener(topics = "invoice-topic")
    public void processInvoice(Invoice invoice) throws InterruptedException {
        logger.info("Processing : " + invoice.toString());
        Thread.sleep(5000);
        InvoiceRepository.addInvoice(invoice);
        logger.info("Done: " + invoice.toString());
    }
}
