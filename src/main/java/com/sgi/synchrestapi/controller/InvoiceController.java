package com.sgi.synchrestapi.controller;

import com.sgi.synchrestapi.InvoiceService;
import com.sgi.synchrestapi.common.Invoice;
import com.sgi.synchrestapi.messaging.Producer;
import com.sgi.synchrestapi.repository.InvoiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
public class InvoiceController {
    private final Logger logger = LoggerFactory.getLogger(InvoiceService.class);

    @PostMapping(path = "/api/invoice")
    public String processInvoice(@RequestBody Invoice newInvoice) {
        logger.info("process new invoice");
        // Produce event
        Producer invoiceProducer = new Producer();
        invoiceProducer.sendInvoice(newInvoice);

        return "Invoice will be process";
    }

    @GetMapping(path = "/api/invoice/{invid}")
    public boolean getInvoiceStatus(String invid) {
        // Jika invoice sudah selesai di kerjakan maka return true
        Invoice findInvoice = InvoiceRepository.getInvoice(invid);
        if(findInvoice != null) {
            return true;
        } else {
            return false;
        }
    }
}
