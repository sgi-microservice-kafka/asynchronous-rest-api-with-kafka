package com.sgi.synchrestapi;

import org.apache.kafka.clients.admin.NewTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.converter.JsonMessageConverter;
import org.springframework.kafka.support.converter.RecordMessageConverter;

@SpringBootApplication
public class InvoiceService {
	private final Logger logger = LoggerFactory.getLogger(InvoiceService.class);
	public static void main(String[] args) {
		SpringApplication.run(InvoiceService.class, args);
	}

	@Bean
	public RecordMessageConverter converter() {
		return new JsonMessageConverter();
	}

	@Bean
	public NewTopic invoiceTopic() {
		String topic = "invoice-topic";
		int numPartition = 3;
		short replicationFactor = 2;
		logger.debug("New topic created:"+topic+ " Partition:"+numPartition+" Replication:"+replicationFactor);

		return new NewTopic(topic, numPartition, replicationFactor);
	}

}
